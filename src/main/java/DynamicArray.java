public class DynamicArray<T> {
    private T[] array;
    private int capacity;
    private int size;

    public DynamicArray(int capacity) {
        this.capacity = capacity;
        array = (T[]) new Object[capacity];
        this.size = 0;
    }

    public DynamicArray() {
        this(10);
    }

    public static void main(String[] args) {
        DynamicArray<String> array = new DynamicArray<>(16);
        for (int i = 1; i <= 20; i++) {
            array.add(String.valueOf(i));
        }
        System.out.println(array);
        System.out.println(array.get(5));
        System.out.println(array.size());

        array.remove(9);
        array.remove(9);
        array.remove(9);
        array.remove(9);
        System.out.println(array);
        System.out.println(array.size());
    }

    public int size() {
        return size;
    }

    public void add(T element) {
        if (size == capacity) {
            capacity = (int) (capacity * 1.5);
            T[] oldArray = array;
            array = (T[]) new Object[capacity];
            System.arraycopy(oldArray, 0, array, 0, size);
        }
        array[++size - 1] = element;
    }

    public T get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public void remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        } else if (index == size - 1) {
            array[size - 1] = null;
        } else {
            System.arraycopy(array, index + 1, array, index, size - 1 - index);
            array[--size] = null;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("DynamicArray : {");
        for (int i = 0; i < size; i++) {
            builder.append(array[i]);
            if ((i == size - 1)) {
                builder.append("}\n");
            } else {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
}
